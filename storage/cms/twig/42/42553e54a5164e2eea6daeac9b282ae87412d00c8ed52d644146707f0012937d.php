<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\local\OpenServer\domains\kaktv/plugins/saurabhdhariwal/comments/components/comments/block.htm */
class __TwigTemplate_e55db223b60bfabb070a3b130fcfe994f099da005227dfc52983d0243c372bb3 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<section class=\"comment-section\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-lg-8 col-md-12\">
                ";
        // line 5
        $context['__cms_partial_params'] = [];
        $context['__cms_partial_params']['post'] = ($context["post_id"] ?? null)        ;
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("commentsPost::form"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 6
        echo "
                <h4><b>КОММЕНТАРИИ(<span id=\"comments-count\">";
        // line 7
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["commentsPost"] ?? null), "count", [], "any", false, false, false, 7), "html", null, true);
        echo "</span>)</b></h4>

                ";
        // line 9
        if ((twig_get_attribute($this->env, $this->source, ($context["commentsPost"] ?? null), "count", [], "any", false, false, false, 9) > 0)) {
            // line 10
            echo "                    <div class=\"comments\">
                        ";
            // line 11
            $context['__cms_partial_params'] = [];
            $context['__cms_partial_params']['post'] = ($context["commentsPost"] ?? null)            ;
            echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("commentsPost::list"            , $context['__cms_partial_params']            , true            );
            unset($context['__cms_partial_params']);
            // line 12
            echo "                    </div>
                ";
        } else {
            // line 14
            echo "                    <div class=\"comments\">
                        <p style=\"text-align: center; margin-top: 20px\" class=\"empty-comment\">Комментарии отсутствуют</p>
                    </div>
                ";
        }
        // line 18
        echo "                
                ";
        // line 19
        if ((twig_get_attribute($this->env, $this->source, ($context["commentsPost"] ?? null), "count", [], "any", false, false, false, 19) > 0)) {
            // line 20
            echo "                    <a class=\"load-more-btn more-comment-btn\" href=\"#\" 
                        data-attach-loading 
                        data-control=\"auto-pager\"
                        data-request=\"onPageComments\"
                        data-request-update=\"'commentsPost::list': '@.comments'\"
                        data-current-page=\"1\"
                        data-last-page=\"";
            // line 26
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["commentsPost"] ?? null), "lastPage", [], "any", false, false, false, 26), "html", null, true);
            echo "\"><b>ПОКАЗАТЬ ЕЩЕ</b></a>
                ";
        }
        // line 28
        echo "            </div>
        </div>
    </div>
</section>

";
    }

    public function getTemplateName()
    {
        return "C:\\local\\OpenServer\\domains\\kaktv/plugins/saurabhdhariwal/comments/components/comments/block.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 28,  89 => 26,  81 => 20,  79 => 19,  76 => 18,  70 => 14,  66 => 12,  61 => 11,  58 => 10,  56 => 9,  51 => 7,  48 => 6,  43 => 5,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<section class=\"comment-section\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-lg-8 col-md-12\">
                {% partial \"commentsPost::form\" post=post_id %}

                <h4><b>КОММЕНТАРИИ(<span id=\"comments-count\">{{commentsPost.count}}</span>)</b></h4>

                {% if(commentsPost.count) > 0 %}
                    <div class=\"comments\">
                        {% partial \"commentsPost::list\" post=commentsPost %}
                    </div>
                {% else %}
                    <div class=\"comments\">
                        <p style=\"text-align: center; margin-top: 20px\" class=\"empty-comment\">Комментарии отсутствуют</p>
                    </div>
                {% endif %}
                
                {% if(commentsPost.count) > 0 %}
                    <a class=\"load-more-btn more-comment-btn\" href=\"#\" 
                        data-attach-loading 
                        data-control=\"auto-pager\"
                        data-request=\"onPageComments\"
                        data-request-update=\"'commentsPost::list': '@.comments'\"
                        data-current-page=\"1\"
                        data-last-page=\"{{ commentsPost.lastPage }}\"><b>ПОКАЗАТЬ ЕЩЕ</b></a>
                {% endif %}
            </div>
        </div>
    </div>
</section>

", "C:\\local\\OpenServer\\domains\\kaktv/plugins/saurabhdhariwal/comments/components/comments/block.htm", "");
    }
}
