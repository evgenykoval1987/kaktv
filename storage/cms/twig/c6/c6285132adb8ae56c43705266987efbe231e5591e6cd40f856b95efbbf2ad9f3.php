<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\local\OpenServer\domains\kaktv/themes/responsiv-clean/partials/site/header.htm */
class __TwigTemplate_33be4baa979a6c138bafcf5e64f630a6f0bde2a26aa956d45717d11ec7c2b656 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"container-fluid position-relative no-side-padding\">
    <a href=\"";
        // line 2
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("home");
        echo "\" class=\"logo\"><img src=\"";
        echo $this->extensions['System\Twig\Extension']->mediaFilter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 2), "logo", [], "any", false, false, false, 2));
        echo "\" alt=\"Logo Image\"></a>

    <div class=\"menu-nav-icon\" data-nav-menu=\"#main-menu\"><i class=\"ion-navicon\"></i></div>

    <ul class=\"main-menu visible-on-click\" id=\"main-menu\">
        <li><a href=\"";
        // line 7
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("home");
        echo "\">Главная</a></li>
        <li><a href=\"";
        // line 8
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("blog/categories");
        echo "\">Категории</a></li>
        <li><a href=\"";
        // line 9
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("discussed");
        echo "\">Обсуждаемое</a></li>
    </ul>

    <div class=\"src-area\">
        <form action=\"";
        // line 13
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("search");
        echo "\" method=\"get\">
            <button class=\"src-btn\" type=\"submit\"><i class=\"ion-ios-search-strong\"></i></button>
            <input class=\"src-input\" type=\"text\" placeholder=\"Поиск\" name=\"q\" autocomplete=\"off\">
        </form>
    </div>

</div>";
    }

    public function getTemplateName()
    {
        return "C:\\local\\OpenServer\\domains\\kaktv/themes/responsiv-clean/partials/site/header.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 13,  58 => 9,  54 => 8,  50 => 7,  40 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"container-fluid position-relative no-side-padding\">
    <a href=\"{{ 'home'|page }}\" class=\"logo\"><img src=\"{{ this.theme.logo|media }}\" alt=\"Logo Image\"></a>

    <div class=\"menu-nav-icon\" data-nav-menu=\"#main-menu\"><i class=\"ion-navicon\"></i></div>

    <ul class=\"main-menu visible-on-click\" id=\"main-menu\">
        <li><a href=\"{{ 'home'|page }}\">Главная</a></li>
        <li><a href=\"{{ 'blog/categories'|page }}\">Категории</a></li>
        <li><a href=\"{{ 'discussed'|page }}\">Обсуждаемое</a></li>
    </ul>

    <div class=\"src-area\">
        <form action=\"{{ 'search' | page }}\" method=\"get\">
            <button class=\"src-btn\" type=\"submit\"><i class=\"ion-ios-search-strong\"></i></button>
            <input class=\"src-input\" type=\"text\" placeholder=\"Поиск\" name=\"q\" autocomplete=\"off\">
        </form>
    </div>

</div>", "C:\\local\\OpenServer\\domains\\kaktv/themes/responsiv-clean/partials/site/header.htm", "");
    }
}
