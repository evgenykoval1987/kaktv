<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\local\OpenServer\domains\kaktv/themes/responsiv-clean/partials/blog/post-without-image.htm */
class __TwigTemplate_41ede96d1be028f08ef5945c5ab14eeef3155b656615b4808b419c03c0ad46e8 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"blog-info\">
\t";
        // line 2
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "categories", [], "any", false, false, false, 2), "count", [], "any", false, false, false, 2)) {
            // line 3
            echo "\t    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "categories", [], "any", false, false, false, 3));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 4
                echo "\t        ";
                if ((twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 4) == 1)) {
                    // line 5
                    echo "\t            <h6 class=\"pre-title\"><a href=\"";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "url", [], "any", false, false, false, 5), "html", null, true);
                    echo "\"><b>";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 5), "html", null, true);
                    echo "</b></a></h6>
\t        ";
                }
                // line 7
                echo "\t    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 8
            echo "\t";
        }
        // line 9
        echo "\t<h4 class=\"title\">
\t\t<a href=\"";
        // line 10
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "url", [], "any", false, false, false, 10), "html", null, true);
        echo "\">
\t\t\t<b>";
        // line 11
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "title", [], "any", false, false, false, 11), "html", null, true);
        echo "</b>
\t\t</a>
\t</h4>
\t<p>
\t\t";
        // line 15
        echo twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "excerpt", [], "any", false, false, false, 15);
        echo "
\t</p>
\t<div class=\"avatar-area\">
\t\t<a class=\"avatar\" href=\"";
        // line 18
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("blog/author", ["slug" => twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "user_id", [], "any", false, false, false, 18)]);
        echo "\">
\t\t\t<img src=\"";
        // line 19
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "user", [], "any", false, false, false, 19), "avatar", [], "any", false, false, false, 19), "path", [], "any", false, false, false, 19), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "user", [], "any", false, false, false, 19), "first_name", [], "any", false, false, false, 19), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "user", [], "any", false, false, false, 19), "last_name", [], "any", false, false, false, 19), "html", null, true);
        echo "\">
\t\t</a>
\t\t<div class=\"right-area\">
\t\t\t<a class=\"name\" href=\"#\"><b>";
        // line 22
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "user", [], "any", false, false, false, 22), "first_name", [], "any", false, false, false, 22), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "user", [], "any", false, false, false, 22), "last_name", [], "any", false, false, false, 22), "html", null, true);
        echo "</b></a>
            <h6 class=\"date\">";
        // line 23
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "published_at", [], "any", false, false, false, 23), "d.m.Y H:i"), "html", null, true);
        echo "</h6>
\t\t</div>
\t</div>
\t";
        // line 26
        $context['__cms_partial_params'] = [];
        $context['__cms_partial_params']['post'] = ($context["post"] ?? null)        ;
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("blog/post-footer"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 27
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "C:\\local\\OpenServer\\domains\\kaktv/themes/responsiv-clean/partials/blog/post-without-image.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  139 => 27,  134 => 26,  128 => 23,  122 => 22,  112 => 19,  108 => 18,  102 => 15,  95 => 11,  91 => 10,  88 => 9,  85 => 8,  71 => 7,  63 => 5,  60 => 4,  42 => 3,  40 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"blog-info\">
\t{% if post.categories.count %}
\t    {% for category in post.categories %}
\t        {% if loop.index == 1 %}
\t            <h6 class=\"pre-title\"><a href=\"{{category.url}}\"><b>{{category.name}}</b></a></h6>
\t        {% endif %}
\t    {% endfor %}
\t{% endif %}
\t<h4 class=\"title\">
\t\t<a href=\"{{ post.url }}\">
\t\t\t<b>{{ post.title }}</b>
\t\t</a>
\t</h4>
\t<p>
\t\t{{ post.excerpt|raw }}
\t</p>
\t<div class=\"avatar-area\">
\t\t<a class=\"avatar\" href=\"{{'blog/author'|page({ slug: post.user_id })}}\">
\t\t\t<img src=\"{{ post.user.avatar.path }}\" alt=\"{{ post.user.first_name }} {{ post.user.last_name }}\">
\t\t</a>
\t\t<div class=\"right-area\">
\t\t\t<a class=\"name\" href=\"#\"><b>{{ post.user.first_name }} {{ post.user.last_name }}</b></a>
            <h6 class=\"date\">{{ post.published_at|date('d.m.Y H:i') }}</h6>
\t\t</div>
\t</div>
\t{% partial 'blog/post-footer' post=post %}
</div>", "C:\\local\\OpenServer\\domains\\kaktv/themes/responsiv-clean/partials/blog/post-without-image.htm", "");
    }
}
