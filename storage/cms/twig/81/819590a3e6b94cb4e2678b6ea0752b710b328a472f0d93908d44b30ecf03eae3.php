<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\local\OpenServer\domains\kaktv/themes/responsiv-clean/partials/blog/post-footer.htm */
class __TwigTemplate_dd757bbf7b80efacadaa4f4706a86a1f456fe6e502ac9f6ed2e1c929813aa5fe extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<ul class=\"post-footer\">
    <li><a href=\"#\" class=\"like ";
        // line 2
        echo (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "userlikes", [], "any", false, false, false, 2), "count", [], "any", false, false, false, 2) != 0)) ? ("active") : (""));
        echo "\" data-post-id=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "id", [], "any", false, false, false, 2), "html", null, true);
        echo "\"><i class=\"ion-heart\"></i><span>";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "likes", [], "any", false, false, false, 2), "count", [], "any", false, false, false, 2), "html", null, true);
        echo "</span></a></li>
    <li><a href=\"#\"><i class=\"ion-chatbubble\"></i>";
        // line 3
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "comments", [], "any", false, false, false, 3), "count", [], "any", false, false, false, 3), "html", null, true);
        echo "</a></li>
    <li><a href=\"#\"><i class=\"ion-eye\"></i>";
        // line 4
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "viewed", [], "any", false, false, false, 4), "html", null, true);
        echo "</a></li>
</ul>";
    }

    public function getTemplateName()
    {
        return "C:\\local\\OpenServer\\domains\\kaktv/themes/responsiv-clean/partials/blog/post-footer.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 4,  48 => 3,  40 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<ul class=\"post-footer\">
    <li><a href=\"#\" class=\"like {{ (post.userlikes.count != 0) ? 'active' : '' }}\" data-post-id=\"{{post.id}}\"><i class=\"ion-heart\"></i><span>{{post.likes.count}}</span></a></li>
    <li><a href=\"#\"><i class=\"ion-chatbubble\"></i>{{post.comments.count}}</a></li>
    <li><a href=\"#\"><i class=\"ion-eye\"></i>{{post.viewed}}</a></li>
</ul>", "C:\\local\\OpenServer\\domains\\kaktv/themes/responsiv-clean/partials/blog/post-footer.htm", "");
    }
}
