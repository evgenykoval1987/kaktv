<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\local\OpenServer\domains\kaktv/plugins/jorgeandrade/subscribe/components/subscriber/default.htm */
class __TwigTemplate_f14b6c38a9c5e96de1326f5395b8b49d24ed78445aada8f4a7bbb4d16764d692 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<h4 class=\"title\"><b>Подписаться</b></h4>
<div class=\"sub-result\"></div>
<div class=\"input-area\">
    <form data-request=\"";
        // line 4
        echo twig_escape_filter($this->env, ($context["__SELF__"] ?? null), "html", null, true);
        echo "::onAddSubscriber\" data-request-update=\"'";
        echo twig_escape_filter($this->env, ($context["__SELF__"] ?? null), "html", null, true);
        echo "::alert': '.sub-result'\" data-attach-loading >
        <input class=\"email-input\" type=\"email\" placeholder=\"Введите Email\" name=\"email\">
        <input type=\"hidden\" name=\"latitude\" id=\"latitude\">
        <input type=\"hidden\" name=\"longitude\" id=\"longitude\">
        <button class=\"submit-btn\" type=\"submit\"><i class=\"icon ion-ios-email-outline\"></i></button>
    </form>
</div>";
    }

    public function getTemplateName()
    {
        return "C:\\local\\OpenServer\\domains\\kaktv/plugins/jorgeandrade/subscribe/components/subscriber/default.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 4,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<h4 class=\"title\"><b>Подписаться</b></h4>
<div class=\"sub-result\"></div>
<div class=\"input-area\">
    <form data-request=\"{{ __SELF__ }}::onAddSubscriber\" data-request-update=\"'{{__SELF__}}::alert': '.sub-result'\" data-attach-loading >
        <input class=\"email-input\" type=\"email\" placeholder=\"Введите Email\" name=\"email\">
        <input type=\"hidden\" name=\"latitude\" id=\"latitude\">
        <input type=\"hidden\" name=\"longitude\" id=\"longitude\">
        <button class=\"submit-btn\" type=\"submit\"><i class=\"icon ion-ios-email-outline\"></i></button>
    </form>
</div>", "C:\\local\\OpenServer\\domains\\kaktv/plugins/jorgeandrade/subscribe/components/subscriber/default.htm", "");
    }
}
