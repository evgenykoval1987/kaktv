<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\local\OpenServer\domains\kaktv/themes/responsiv-clean/pages/home.htm */
class __TwigTemplate_8716416be5bcb491b1bd00b87435b3fdbf9ba4e53b8503c105eee341739419fd extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"slider\" style=\"background-image: url('";
        echo $this->extensions['System\Twig\Extension']->mediaFilter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 1), "banner", [], "any", false, false, false, 1));
        echo "');\"></div>
<section class=\"blog-area section\">
    <div class=\"container\">
        <div class=\"row\" id=\"home-posts\"> 
            ";
        // line 5
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("home/posts"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 6
        echo "        </div>
        
        ";
        // line 8
        if ((twig_get_attribute($this->env, $this->source, ($context["posts"] ?? null), "lastPage", [], "any", false, false, false, 8) != twig_get_attribute($this->env, $this->source, ($context["posts"] ?? null), "currentPage", [], "any", false, false, false, 8))) {
            // line 9
            echo "        <a class=\"load-more-btn\" href=\"#\" 
                data-attach-loading 
                data-control=\"auto-pager\"
                data-request=\"onPagePosts\"
                data-request-update=\"'home/posts': '@#home-posts'\"
                data-current-page=\"1\"
                data-last-page=\"";
            // line 15
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["posts"] ?? null), "lastPage", [], "any", false, false, false, 15), "html", null, true);
            echo "\"><b>ПОКАЗАТЬ ЕЩЕ</b></a>
        ";
        }
        // line 17
        echo "        <!--<div class=\"row\">
            <div
                class=\"text-center\"
                data-control=\"auto-pager\"
                data-request=\"onPagePosts\"
                data-request-update=\"'home/posts': '@#home-posts'\"
                data-current-page=\"1\"
                data-last-page=\"";
        // line 24
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["posts"] ?? null), "lastPage", [], "any", false, false, false, 24), "html", null, true);
        echo "\">
                <a class=\"text-muted oc-loading\">Fetching posts..</a>
            </div>
        </div>-->
    </div>
</section>";
    }

    public function getTemplateName()
    {
        return "C:\\local\\OpenServer\\domains\\kaktv/themes/responsiv-clean/pages/home.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  77 => 24,  68 => 17,  63 => 15,  55 => 9,  53 => 8,  49 => 6,  45 => 5,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"slider\" style=\"background-image: url('{{ this.theme.banner|media }}');\"></div>
<section class=\"blog-area section\">
    <div class=\"container\">
        <div class=\"row\" id=\"home-posts\"> 
            {% partial 'home/posts' %}
        </div>
        
        {% if posts.lastPage != posts.currentPage%}
        <a class=\"load-more-btn\" href=\"#\" 
                data-attach-loading 
                data-control=\"auto-pager\"
                data-request=\"onPagePosts\"
                data-request-update=\"'home/posts': '@#home-posts'\"
                data-current-page=\"1\"
                data-last-page=\"{{ posts.lastPage }}\"><b>ПОКАЗАТЬ ЕЩЕ</b></a>
        {% endif %}
        <!--<div class=\"row\">
            <div
                class=\"text-center\"
                data-control=\"auto-pager\"
                data-request=\"onPagePosts\"
                data-request-update=\"'home/posts': '@#home-posts'\"
                data-current-page=\"1\"
                data-last-page=\"{{ posts.lastPage }}\">
                <a class=\"text-muted oc-loading\">Fetching posts..</a>
            </div>
        </div>-->
    </div>
</section>", "C:\\local\\OpenServer\\domains\\kaktv/themes/responsiv-clean/pages/home.htm", "");
    }
}
