<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\local\OpenServer\domains\kaktv/themes/responsiv-clean/partials/blog/post-big.htm */
class __TwigTemplate_08fb58361e07309adb7aafa2513c0f5208571834c1a254c671272a5cf5b592ba extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"blog-image\">
\t<img src=\"";
        // line 2
        echo $this->extensions['System\Twig\Extension']->mediaFilter(twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "image_v", [], "any", false, false, false, 2));
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "title", [], "any", false, false, false, 2), "html", null, true);
        echo "\">
</div>
<div class=\"blog-info\">
\t";
        // line 5
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "categories", [], "any", false, false, false, 5), "count", [], "any", false, false, false, 5)) {
            // line 6
            echo "\t    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "categories", [], "any", false, false, false, 6));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 7
                echo "\t        ";
                if ((twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 7) == 1)) {
                    // line 8
                    echo "\t            <h6 class=\"pre-title\"><a href=\"";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "url", [], "any", false, false, false, 8), "html", null, true);
                    echo "\"><b>";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 8), "html", null, true);
                    echo "</b></a></h6>
\t        ";
                }
                // line 10
                echo "\t    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 11
            echo "\t";
        }
        // line 12
        echo "\t
\t<h4 class=\"title\">
\t\t<a href=\"";
        // line 14
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "url", [], "any", false, false, false, 14), "html", null, true);
        echo "\">
\t\t\t<b>";
        // line 15
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "title", [], "any", false, false, false, 15), "html", null, true);
        echo "</b>
\t\t</a>
\t</h4>
\t<p>
\t\t";
        // line 19
        echo twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "excerpt", [], "any", false, false, false, 19);
        echo "
\t</p>
\t<div class=\"avatar-area\">
\t\t<a class=\"avatar\" href=\"";
        // line 22
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("blog/author", ["slug" => twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "user_id", [], "any", false, false, false, 22)]);
        echo "\">
\t\t\t<img src=\"";
        // line 23
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "user", [], "any", false, false, false, 23), "avatar", [], "any", false, false, false, 23), "path", [], "any", false, false, false, 23), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "user", [], "any", false, false, false, 23), "first_name", [], "any", false, false, false, 23), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "user", [], "any", false, false, false, 23), "last_name", [], "any", false, false, false, 23), "html", null, true);
        echo "\">
\t\t</a>
\t\t<div class=\"right-area\">
\t\t\t<a class=\"name\" href=\"";
        // line 26
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("blog/author", ["slug" => twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "user_id", [], "any", false, false, false, 26)]);
        echo "\"><b>";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "user", [], "any", false, false, false, 26), "first_name", [], "any", false, false, false, 26), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "user", [], "any", false, false, false, 26), "last_name", [], "any", false, false, false, 26), "html", null, true);
        echo "</b></a>
            <h6 class=\"date\">";
        // line 27
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "published_at", [], "any", false, false, false, 27), "d.m.Y H:i"), "html", null, true);
        echo "</h6>
\t\t</div>
\t</div>
\t";
        // line 30
        $context['__cms_partial_params'] = [];
        $context['__cms_partial_params']['post'] = ($context["post"] ?? null)        ;
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("blog/post-footer"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 31
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "C:\\local\\OpenServer\\domains\\kaktv/themes/responsiv-clean/partials/blog/post-big.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  150 => 31,  145 => 30,  139 => 27,  131 => 26,  121 => 23,  117 => 22,  111 => 19,  104 => 15,  100 => 14,  96 => 12,  93 => 11,  79 => 10,  71 => 8,  68 => 7,  50 => 6,  48 => 5,  40 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"blog-image\">
\t<img src=\"{{post.image_v|media}}\" alt=\"{{ post.title }}\">
</div>
<div class=\"blog-info\">
\t{% if post.categories.count %}
\t    {% for category in post.categories %}
\t        {% if loop.index == 1 %}
\t            <h6 class=\"pre-title\"><a href=\"{{category.url}}\"><b>{{category.name}}</b></a></h6>
\t        {% endif %}
\t    {% endfor %}
\t{% endif %}
\t
\t<h4 class=\"title\">
\t\t<a href=\"{{ post.url }}\">
\t\t\t<b>{{ post.title }}</b>
\t\t</a>
\t</h4>
\t<p>
\t\t{{ post.excerpt|raw }}
\t</p>
\t<div class=\"avatar-area\">
\t\t<a class=\"avatar\" href=\"{{'blog/author'|page({ slug: post.user_id })}}\">
\t\t\t<img src=\"{{ post.user.avatar.path }}\" alt=\"{{ post.user.first_name }} {{ post.user.last_name }}\">
\t\t</a>
\t\t<div class=\"right-area\">
\t\t\t<a class=\"name\" href=\"{{'blog/author'|page({ slug: post.user_id })}}\"><b>{{ post.user.first_name }} {{ post.user.last_name }}</b></a>
            <h6 class=\"date\">{{ post.published_at|date('d.m.Y H:i') }}</h6>
\t\t</div>
\t</div>
\t{% partial 'blog/post-footer' post=post %}
</div>", "C:\\local\\OpenServer\\domains\\kaktv/themes/responsiv-clean/partials/blog/post-big.htm", "");
    }
}
