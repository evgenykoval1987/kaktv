<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\local\OpenServer\domains\kaktv/plugins/bedard/blogtags/components/blogtags/default.htm */
class __TwigTemplate_dec3fd67e9c6a527e221d7327e6f6ff07ee60cc1ed9e171c5da8c5c15dddd590 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<ul class=\"tags\">
    ";
        // line 2
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["__SELF__"] ?? null), "tags", [], "any", false, false, false, 2));
        foreach ($context['_seq'] as $context["_key"] => $context["tag"]) {
            // line 3
            echo "        <li><a href=\"";
            echo $this->extensions['Cms\Twig\Extension']->pageFilter("blog/tag", ["slug" => twig_get_attribute($this->env, $this->source, $context["tag"], "name", [], "any", false, false, false, 3)]);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["tag"], "name", [], "any", false, false, false, 3), "html", null, true);
            echo "</a></li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tag'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 5
        echo "</ul>
";
    }

    public function getTemplateName()
    {
        return "C:\\local\\OpenServer\\domains\\kaktv/plugins/bedard/blogtags/components/blogtags/default.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  55 => 5,  44 => 3,  40 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<ul class=\"tags\">
    {% for tag in __SELF__.tags %}
        <li><a href=\"{{'blog/tag'|page({ slug: tag.name })}}\">{{ tag.name }}</a></li>
    {% endfor %}
</ul>
", "C:\\local\\OpenServer\\domains\\kaktv/plugins/bedard/blogtags/components/blogtags/default.htm", "");
    }
}
