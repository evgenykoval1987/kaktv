<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\local\OpenServer\domains\kaktv/themes/responsiv-clean/pages/blog/post.htm */
class __TwigTemplate_bc8f70f7dc16829f2efb6dee8292500f6f779df5c5d3df08a61c24f0ce1b62a7 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["image"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "featured_images", [], "any", false, false, false, 1), "first", [], "any", false, false, false, 1);
        // line 2
        $context["nextPost"] = twig_get_attribute($this->env, $this->source, ($context["blogPost"] ?? null), "nextPost", [], "any", false, false, false, 2);
        // line 3
        $context["lastPost"] = twig_get_attribute($this->env, $this->source, ($context["blogPost"] ?? null), "previousPost", [], "any", false, false, false, 3);
        // line 4
        echo "
";
        // line 5
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "categories", [], "any", false, false, false, 5), "count", [], "any", false, false, false, 5)) {
            // line 6
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "categories", [], "any", false, false, false, 6));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 7
                echo "        ";
                if ((twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 7) == 1)) {
                    // line 8
                    echo "            <div class=\"slider\">
                <div class=\"display-table  center-text\">
                    <h1 class=\"title display-table-cell\" style=\"background-image: url('";
                    // line 10
                    echo $this->extensions['System\Twig\Extension']->mediaFilter(twig_get_attribute($this->env, $this->source, $context["category"], "banner", [], "any", false, false, false, 10));
                    echo "')\"><b>";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 10), "html", null, true);
                    echo "</b></h1>
                </div>
            </div>
        ";
                }
                // line 14
                echo "    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        // line 16
        echo "<section class=\"post-area section\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-lg-8 col-md-12 no-right-padding\">
                <div class=\"main-post\">
                    <div class=\"blog-post-inner\">
                        <div class=\"post-info\">
                            <div class=\"left-area\">
                                <a class=\"avatar\" href=\"";
        // line 24
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("blog/author", ["slug" => twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "user_id", [], "any", false, false, false, 24)]);
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "user", [], "any", false, false, false, 24), "avatar", [], "any", false, false, false, 24), "path", [], "any", false, false, false, 24), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "user", [], "any", false, false, false, 24), "first_name", [], "any", false, false, false, 24), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "user", [], "any", false, false, false, 24), "last_name", [], "any", false, false, false, 24), "html", null, true);
        echo "\"></a>
                            </div>
                            <div class=\"middle-area\">
                                <a class=\"name\" href=\"";
        // line 27
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("blog/author", ["slug" => twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "user_id", [], "any", false, false, false, 27)]);
        echo "\"><b>";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "user", [], "any", false, false, false, 27), "first_name", [], "any", false, false, false, 27), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "user", [], "any", false, false, false, 27), "last_name", [], "any", false, false, false, 27), "html", null, true);
        echo "</b></a>
                                <h6 class=\"date\">";
        // line 28
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "published_at", [], "any", false, false, false, 28), "d.m.Y H:i"), "html", null, true);
        echo "</h6>
                            </div>
                        </div>
                        <h3 class=\"title\"><b>";
        // line 31
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "title", [], "any", false, false, false, 31), "html", null, true);
        echo "</b></h3>
                        ";
        // line 32
        echo twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "content_html", [], "any", false, false, false, 32);
        echo "

                        ";
        // line 34
        if (($context["blogTags"] ?? null)) {
            // line 35
            echo "                            ";
            $context['__cms_partial_params'] = [];
            echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("blogTags::default"            , $context['__cms_partial_params']            , true            );
            unset($context['__cms_partial_params']);
            // line 36
            echo "                        ";
        }
        // line 37
        echo "                    </div>
                    <div class=\"post-icons-area\">
                        <ul class=\"post-icons\">
                            <li><a href=\"#\" class=\"like ";
        // line 40
        echo (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "userlikes", [], "any", false, false, false, 40), "count", [], "any", false, false, false, 40) != 0)) ? ("active") : (""));
        echo "\" data-post-id=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "id", [], "any", false, false, false, 40), "html", null, true);
        echo "\"><i class=\"ion-heart\"></i><span>";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "likes", [], "any", false, false, false, 40), "count", [], "any", false, false, false, 40), "html", null, true);
        echo "</span></a></li>
                            <li><a href=\"#\"><i class=\"ion-chatbubble\"></i>";
        // line 41
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "comments", [], "any", false, false, false, 41), "count", [], "any", false, false, false, 41), "html", null, true);
        echo "</a></li>
                            <li><a href=\"#\"><i class=\"ion-eye\"></i>";
        // line 42
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "viewed", [], "any", false, false, false, 42), "html", null, true);
        echo "</a></li>
                        </ul>
                        ";
        // line 44
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->componentFunction("ssbuttonsssb"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        // line 45
        echo "                        
                    </div>
                    <div class=\"post-footer post-info\">
                        <div class=\"left-area\">
                            <a class=\"avatar\" href=\"#\"><img src=\"";
        // line 49
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "user", [], "any", false, false, false, 49), "avatar", [], "any", false, false, false, 49), "path", [], "any", false, false, false, 49), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "user", [], "any", false, false, false, 49), "first_name", [], "any", false, false, false, 49), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "user", [], "any", false, false, false, 49), "last_name", [], "any", false, false, false, 49), "html", null, true);
        echo "\"></a>
                        </div>
                        <div class=\"middle-area\">
                            <a class=\"name\" href=\"#\"><b>";
        // line 52
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "user", [], "any", false, false, false, 52), "first_name", [], "any", false, false, false, 52), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "user", [], "any", false, false, false, 52), "last_name", [], "any", false, false, false, 52), "html", null, true);
        echo "</b></a>
                            <h6 class=\"date\">";
        // line 53
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "published_at", [], "any", false, false, false, 53), "d.m.Y H:i"), "html", null, true);
        echo "</h6>
                        </div>
                    </div>
                </div>
            </div>
            <div class=\"col-lg-4 col-md-12 no-left-padding\">
                <div class=\"single-post info-area\">
                    <div class=\"sidebar-area subscribe-area\">
                        ";
        // line 61
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->componentFunction("formSubscribe"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        // line 62
        echo "                    </div>
                    ";
        // line 63
        if (($context["all_tags"] ?? null)) {
            // line 64
            echo "                        <div class=\"tag-area\">
                            <h4 class=\"title\"><b>Теги</b></h4>
                            <ul>
                                ";
            // line 67
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["all_tags"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["tag"]) {
                // line 68
                echo "                                    <li><a href=\"";
                echo $this->extensions['Cms\Twig\Extension']->pageFilter("blog/tag", ["slug" => twig_get_attribute($this->env, $this->source, $context["tag"], "name", [], "any", false, false, false, 68)]);
                echo "\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["tag"], "name", [], "any", false, false, false, 68), "html", null, true);
                echo "</a></li>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tag'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 70
            echo "                            </ul>
                        </div>
                    ";
        }
        // line 73
        echo "                </div>
            </div>
        </div>
    </div>
</section>
<!--";
        // line 78
        if (($context["related"] ?? null)) {
            // line 79
            echo "<section class=\"recomended-area section\">
    <div class=\"container\">
        <div class=\"row\">
            ";
            // line 82
            $context['__cms_partial_params'] = [];
            $context['__cms_partial_params']['posts'] = ($context["related"] ?? null)            ;
            echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("blog/posts"            , $context['__cms_partial_params']            , true            );
            unset($context['__cms_partial_params']);
            // line 83
            echo "        </div>
    </div>
</section>
";
        }
        // line 86
        echo "-->
";
        // line 87
        if (($context["blogRelated"] ?? null)) {
            // line 88
            echo "    <section class=\"recomended-area section\">
        <div class=\"container\">
            <div class=\"row\">
                ";
            // line 91
            $context['__cms_partial_params'] = [];
            $context['__cms_partial_params']['posts'] = twig_get_attribute($this->env, $this->source, ($context["blogRelated"] ?? null), "posts", [], "any", false, false, false, 91)            ;
            echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("blog/posts"            , $context['__cms_partial_params']            , true            );
            unset($context['__cms_partial_params']);
            // line 92
            echo "            </div>
        </div>
    </section>
";
        }
        // line 96
        $context['__cms_partial_params'] = [];
        $context['__cms_partial_params']['post'] = ($context["commentsPost"] ?? null)        ;
        $context['__cms_partial_params']['post_id'] = twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "id", [], "any", false, false, false, 96)        ;
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("commentsPost::block"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
    }

    public function getTemplateName()
    {
        return "C:\\local\\OpenServer\\domains\\kaktv/themes/responsiv-clean/pages/blog/post.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  291 => 96,  285 => 92,  280 => 91,  275 => 88,  273 => 87,  270 => 86,  264 => 83,  259 => 82,  254 => 79,  252 => 78,  245 => 73,  240 => 70,  229 => 68,  225 => 67,  220 => 64,  218 => 63,  215 => 62,  211 => 61,  200 => 53,  194 => 52,  184 => 49,  178 => 45,  174 => 44,  169 => 42,  165 => 41,  157 => 40,  152 => 37,  149 => 36,  144 => 35,  142 => 34,  137 => 32,  133 => 31,  127 => 28,  119 => 27,  107 => 24,  97 => 16,  82 => 14,  73 => 10,  69 => 8,  66 => 7,  48 => 6,  46 => 5,  43 => 4,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set image = post.featured_images.first %}
{% set nextPost = blogPost.nextPost %}
{% set lastPost = blogPost.previousPost %}

{% if post.categories.count %}
    {% for category in post.categories %}
        {% if loop.index == 1 %}
            <div class=\"slider\">
                <div class=\"display-table  center-text\">
                    <h1 class=\"title display-table-cell\" style=\"background-image: url('{{category.banner|media}}')\"><b>{{category.name}}</b></h1>
                </div>
            </div>
        {% endif %}
    {% endfor %}
{% endif %}
<section class=\"post-area section\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-lg-8 col-md-12 no-right-padding\">
                <div class=\"main-post\">
                    <div class=\"blog-post-inner\">
                        <div class=\"post-info\">
                            <div class=\"left-area\">
                                <a class=\"avatar\" href=\"{{'blog/author'|page({ slug: post.user_id })}}\"><img src=\"{{ post.user.avatar.path }}\" alt=\"{{ post.user.first_name }} {{ post.user.last_name }}\"></a>
                            </div>
                            <div class=\"middle-area\">
                                <a class=\"name\" href=\"{{'blog/author'|page({ slug: post.user_id })}}\"><b>{{ post.user.first_name }} {{ post.user.last_name }}</b></a>
                                <h6 class=\"date\">{{ post.published_at|date('d.m.Y H:i') }}</h6>
                            </div>
                        </div>
                        <h3 class=\"title\"><b>{{ post.title }}</b></h3>
                        {{ post.content_html|raw }}

                        {% if blogTags %}
                            {% partial \"blogTags::default\" %}
                        {% endif %}
                    </div>
                    <div class=\"post-icons-area\">
                        <ul class=\"post-icons\">
                            <li><a href=\"#\" class=\"like {{ (post.userlikes.count != 0) ? 'active' : '' }}\" data-post-id=\"{{post.id}}\"><i class=\"ion-heart\"></i><span>{{post.likes.count}}</span></a></li>
                            <li><a href=\"#\"><i class=\"ion-chatbubble\"></i>{{post.comments.count}}</a></li>
                            <li><a href=\"#\"><i class=\"ion-eye\"></i>{{post.viewed}}</a></li>
                        </ul>
                        {% component 'ssbuttonsssb' %}
                        
                    </div>
                    <div class=\"post-footer post-info\">
                        <div class=\"left-area\">
                            <a class=\"avatar\" href=\"#\"><img src=\"{{ post.user.avatar.path }}\" alt=\"{{ post.user.first_name }} {{ post.user.last_name }}\"></a>
                        </div>
                        <div class=\"middle-area\">
                            <a class=\"name\" href=\"#\"><b>{{ post.user.first_name }} {{ post.user.last_name }}</b></a>
                            <h6 class=\"date\">{{ post.published_at|date('d.m.Y H:i') }}</h6>
                        </div>
                    </div>
                </div>
            </div>
            <div class=\"col-lg-4 col-md-12 no-left-padding\">
                <div class=\"single-post info-area\">
                    <div class=\"sidebar-area subscribe-area\">
                        {% component 'formSubscribe' %}
                    </div>
                    {% if all_tags %}
                        <div class=\"tag-area\">
                            <h4 class=\"title\"><b>Теги</b></h4>
                            <ul>
                                {% for tag in all_tags %}
                                    <li><a href=\"{{'blog/tag'|page({ slug: tag.name })}}\">{{ tag.name }}</a></li>
                                {% endfor %}
                            </ul>
                        </div>
                    {% endif %}
                </div>
            </div>
        </div>
    </div>
</section>
<!--{% if related %}
<section class=\"recomended-area section\">
    <div class=\"container\">
        <div class=\"row\">
            {% partial 'blog/posts' posts=related %}
        </div>
    </div>
</section>
{% endif %}-->
{% if blogRelated %}
    <section class=\"recomended-area section\">
        <div class=\"container\">
            <div class=\"row\">
                {% partial 'blog/posts' posts=blogRelated.posts %}
            </div>
        </div>
    </section>
{% endif %}
{% partial \"commentsPost::block\" post=commentsPost post_id=post.id %}", "C:\\local\\OpenServer\\domains\\kaktv/themes/responsiv-clean/pages/blog/post.htm", "");
    }
}
