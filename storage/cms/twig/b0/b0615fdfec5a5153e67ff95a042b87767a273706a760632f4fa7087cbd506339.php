<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\local\OpenServer\domains\kaktv/themes/responsiv-clean/partials/blog/post.htm */
class __TwigTemplate_6b546cce842c950d8571ada7f018fcf6d481e5ce0a9869ce8a6b458c17984903 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["image"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "featured_images", [], "any", false, false, false, 1), "first", [], "any", false, false, false, 1);
        // line 2
        echo "<div class=\"blog-image\">
    <img src=\"";
        // line 3
        echo $this->extensions['System\Twig\Extension']->mediaFilter(twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "image_g", [], "any", false, false, false, 3));
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "title", [], "any", false, false, false, 3), "html", null, true);
        echo "\">
</div>
<a class=\"avatar\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "url", [], "any", false, false, false, 5), "html", null, true);
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "user", [], "any", false, false, false, 5), "avatar", [], "any", false, false, false, 5), "path", [], "any", false, false, false, 5), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "user", [], "any", false, false, false, 5), "first_name", [], "any", false, false, false, 5), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "user", [], "any", false, false, false, 5), "last_name", [], "any", false, false, false, 5), "html", null, true);
        echo "\"></a>
<div class=\"blog-info\">
    <h4 class=\"title\"><a href=\"";
        // line 7
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "url", [], "any", false, false, false, 7), "html", null, true);
        echo "\"><b>";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "title", [], "any", false, false, false, 7), "html", null, true);
        echo "</b></a></h4>
    ";
        // line 8
        $context['__cms_partial_params'] = [];
        $context['__cms_partial_params']['post'] = ($context["post"] ?? null)        ;
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("blog/post-footer"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 9
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "C:\\local\\OpenServer\\domains\\kaktv/themes/responsiv-clean/partials/blog/post.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  71 => 9,  66 => 8,  60 => 7,  49 => 5,  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set image = post.featured_images.first %}
<div class=\"blog-image\">
    <img src=\"{{post.image_g|media}}\" alt=\"{{ post.title }}\">
</div>
<a class=\"avatar\" href=\"{{ post.url }}\"><img src=\"{{ post.user.avatar.path }}\" alt=\"{{ post.user.first_name }} {{ post.user.last_name }}\"></a>
<div class=\"blog-info\">
    <h4 class=\"title\"><a href=\"{{ post.url }}\"><b>{{ post.title }}</b></a></h4>
    {% partial 'blog/post-footer' post=post %}
</div>", "C:\\local\\OpenServer\\domains\\kaktv/themes/responsiv-clean/partials/blog/post.htm", "");
    }
}
