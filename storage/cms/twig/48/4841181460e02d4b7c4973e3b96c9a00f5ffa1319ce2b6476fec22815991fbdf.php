<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\local\OpenServer\domains\kaktv/plugins/saurabhdhariwal/comments/components/comments/list.htm */
class __TwigTemplate_8d71ab4dbfcc70cb2562a0cbe71260ed15ed7b36181e5baab519d40c3e93efe1 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["posts"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 2
            echo "    <div class=\"commnets-area\">
        ";
            // line 3
            $context['__cms_partial_params'] = [];
            $context['__cms_partial_params']['post'] = $context["post"]            ;
            echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("commentsPost::item"            , $context['__cms_partial_params']            , true            );
            unset($context['__cms_partial_params']);
            // line 4
            echo "        ";
            if (twig_get_attribute($this->env, $this->source, $context["post"], "children", [], "any", false, false, false, 4)) {
                // line 5
                echo "            ";
                $context['__cms_partial_params'] = [];
                $context['__cms_partial_params']['posts'] = twig_get_attribute($this->env, $this->source, $context["post"], "children", [], "any", false, false, false, 5)                ;
                echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("commentsPost::clist"                , $context['__cms_partial_params']                , true                );
                unset($context['__cms_partial_params']);
                // line 6
                echo "        ";
            }
            // line 7
            echo "    </div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 9
        echo "<!--<li>
    <div class=\"comment\" id=\"comment-";
        // line 10
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "id", [], "any", false, false, false, 10), "html", null, true);
        echo "\">
        <div class=\"comment-avatar\">
            ";
        // line 12
        echo twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "avatar", [], "any", false, false, false, 12);
        echo "
        </div>
        <div class=\"comment-content\">
            <div class=\"comment-header\">
                <span class=\"comment-name\">";
        // line 16
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "name", [], "any", false, false, false, 16), "html", null, true);
        echo "</span>
                <a class=\"comment-date\" href=\"#comment-";
        // line 17
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "id", [], "any", false, false, false, 17), "html", null, true);
        echo "\">
                    ";
        // line 18
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "created_at", [], "any", false, false, false, 18), "diffForHumans", [], "any", false, false, false, 18), "html", null, true);
        echo "
                </a>
            </div>
            <div>";
        // line 21
        echo twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "content", [], "any", false, false, false, 21);
        echo "</div>
            <div class=\"comment-footer\">
                <a class=\"comment-reply\" onclick=\"Comment.replay(event,'";
        // line 23
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "id", [], "any", false, false, false, 23), "html", null, true);
        echo "')\">Reply</a>
            </div>
        </div>
        <div class=\"clearfix\"></div>
    </div>

    ";
        // line 29
        if (twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "children", [], "any", false, false, false, 29)) {
            // line 30
            echo "    <ul>
        ";
            // line 31
            $context['__cms_partial_params'] = [];
            $context['__cms_partial_params']['posts'] = twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "children", [], "any", false, false, false, 31)            ;
            echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("commentsPost::list"            , $context['__cms_partial_params']            , true            );
            unset($context['__cms_partial_params']);
            // line 32
            echo "    </ul>
    ";
        }
        // line 34
        echo "</li>-->";
    }

    public function getTemplateName()
    {
        return "C:\\local\\OpenServer\\domains\\kaktv/plugins/saurabhdhariwal/comments/components/comments/list.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  125 => 34,  121 => 32,  116 => 31,  113 => 30,  111 => 29,  102 => 23,  97 => 21,  91 => 18,  87 => 17,  83 => 16,  76 => 12,  71 => 10,  68 => 9,  61 => 7,  58 => 6,  52 => 5,  49 => 4,  44 => 3,  41 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% for post in posts %}
    <div class=\"commnets-area\">
        {% partial \"commentsPost::item\" post=post %}
        {% if post.children %}
            {% partial \"commentsPost::clist\" posts=post.children %}
        {% endif %}
    </div>
{% endfor %}
<!--<li>
    <div class=\"comment\" id=\"comment-{{post.id}}\">
        <div class=\"comment-avatar\">
            {{post.avatar|raw}}
        </div>
        <div class=\"comment-content\">
            <div class=\"comment-header\">
                <span class=\"comment-name\">{{post.name}}</span>
                <a class=\"comment-date\" href=\"#comment-{{post.id}}\">
                    {{post.created_at.diffForHumans}}
                </a>
            </div>
            <div>{{post.content|raw}}</div>
            <div class=\"comment-footer\">
                <a class=\"comment-reply\" onclick=\"Comment.replay(event,'{{post.id}}')\">Reply</a>
            </div>
        </div>
        <div class=\"clearfix\"></div>
    </div>

    {% if post.children %}
    <ul>
        {% partial \"commentsPost::list\" posts=post.children %}
    </ul>
    {% endif %}
</li>-->", "C:\\local\\OpenServer\\domains\\kaktv/plugins/saurabhdhariwal/comments/components/comments/list.htm", "");
    }
}
