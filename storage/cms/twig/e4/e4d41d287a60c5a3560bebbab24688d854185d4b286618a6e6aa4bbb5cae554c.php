<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\local\OpenServer\domains\kaktv/themes/responsiv-clean/partials/site/footer.htm */
class __TwigTemplate_65e1aad5fabd10f592cc1d994fcac08b59676e58c6aa56e438e3e610d8e5846a extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<footer>
\t<div class=\"container\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-lg-4 col-md-6\">
\t\t\t\t<div class=\"footer-section\">
\t\t\t\t\t<a class=\"logo\" href=\"";
        // line 6
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("home");
        echo "\"><img src=\"";
        echo $this->extensions['System\Twig\Extension']->mediaFilter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 6), "logo", [], "any", false, false, false, 6));
        echo "\" alt=\"Logo Image\"></a>
\t\t\t\t\t<ul class=\"icons\">
\t\t\t\t\t\t";
        // line 8
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 8), "facebook_url", [], "any", false, false, false, 8)) {
            // line 9
            echo "\t\t\t\t\t\t\t<li><a href=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 9), "facebook_url", [], "any", false, false, false, 9), "html", null, true);
            echo "\"><i class=\"ion-social-facebook-outline\"></i></a></li>
\t\t\t\t\t\t";
        }
        // line 11
        echo "\t\t\t\t\t\t";
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 11), "twitter_url", [], "any", false, false, false, 11)) {
            // line 12
            echo "\t\t\t\t\t\t\t<li><a href=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 12), "twitter_url", [], "any", false, false, false, 12), "html", null, true);
            echo "\"><i class=\"ion-social-twitter-outline\"></i></a></li>
\t\t\t\t\t\t";
        }
        // line 14
        echo "\t\t\t\t\t\t";
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 14), "instagram_url", [], "any", false, false, false, 14)) {
            // line 15
            echo "\t\t\t\t\t\t\t<li><a href=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 15), "instagram_url", [], "any", false, false, false, 15), "html", null, true);
            echo "\"><i class=\"ion-social-instagram-outline\"></i></a></li>
\t\t\t\t\t\t";
        }
        // line 17
        echo "\t\t\t\t\t\t";
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 17), "vk_url", [], "any", false, false, false, 17)) {
            // line 18
            echo "\t\t\t\t\t\t\t<li><a href=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 18), "vk_url", [], "any", false, false, false, 18), "html", null, true);
            echo "\"><i class=\"ion-social-vimeo-outline\"></i></a></li>
\t\t\t\t\t\t";
        }
        // line 20
        echo "\t\t\t\t\t\t";
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 20), "pinterest_url", [], "any", false, false, false, 20)) {
            // line 21
            echo "\t\t\t\t\t\t\t<li><a href=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 21), "pinterest_url", [], "any", false, false, false, 21), "html", null, true);
            echo "\"><i class=\"ion-social-pinterest-outline\"></i></a></li>
\t\t\t\t\t\t";
        }
        // line 23
        echo "\t\t\t\t\t</ul>

\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"col-lg-4 col-md-6\">
\t\t\t\t\t<div class=\"footer-section\">
\t\t\t\t\t<h4 class=\"title\"><b>Категории</b></h4>
\t\t\t\t\t
\t\t\t\t\t";
        // line 31
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_array_batch(($context["categories"] ?? null), 3));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 32
            echo "\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t";
            // line 33
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["category"]);
            foreach ($context['_seq'] as $context["_key"] => $context["column"]) {
                // line 34
                echo "\t\t\t\t\t\t\t\t<li><a href=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["column"], "url", [], "any", false, false, false, 34), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["column"], "name", [], "any", false, false, false, 34), "html", null, true);
                echo "</a></li>
\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['column'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 36
            echo "\t\t\t\t\t\t</ul>
\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 38
        echo "\t\t\t\t\t
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"col-lg-4 col-md-6\">
\t\t\t\t<div class=\"footer-section\">
\t\t\t\t\t
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
</footer>";
    }

    public function getTemplateName()
    {
        return "C:\\local\\OpenServer\\domains\\kaktv/themes/responsiv-clean/partials/site/footer.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  134 => 38,  127 => 36,  116 => 34,  112 => 33,  109 => 32,  105 => 31,  95 => 23,  89 => 21,  86 => 20,  80 => 18,  77 => 17,  71 => 15,  68 => 14,  62 => 12,  59 => 11,  53 => 9,  51 => 8,  44 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<footer>
\t<div class=\"container\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-lg-4 col-md-6\">
\t\t\t\t<div class=\"footer-section\">
\t\t\t\t\t<a class=\"logo\" href=\"{{ 'home'|page }}\"><img src=\"{{ this.theme.logo|media }}\" alt=\"Logo Image\"></a>
\t\t\t\t\t<ul class=\"icons\">
\t\t\t\t\t\t{% if this.theme.facebook_url %}
\t\t\t\t\t\t\t<li><a href=\"{{this.theme.facebook_url}}\"><i class=\"ion-social-facebook-outline\"></i></a></li>
\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t{% if this.theme.twitter_url %}
\t\t\t\t\t\t\t<li><a href=\"{{this.theme.twitter_url}}\"><i class=\"ion-social-twitter-outline\"></i></a></li>
\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t{% if this.theme.instagram_url %}
\t\t\t\t\t\t\t<li><a href=\"{{this.theme.instagram_url}}\"><i class=\"ion-social-instagram-outline\"></i></a></li>
\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t{% if this.theme.vk_url %}
\t\t\t\t\t\t\t<li><a href=\"{{this.theme.vk_url}}\"><i class=\"ion-social-vimeo-outline\"></i></a></li>
\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t{% if this.theme.pinterest_url %}
\t\t\t\t\t\t\t<li><a href=\"{{this.theme.pinterest_url}}\"><i class=\"ion-social-pinterest-outline\"></i></a></li>
\t\t\t\t\t\t{% endif %}
\t\t\t\t\t</ul>

\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"col-lg-4 col-md-6\">
\t\t\t\t\t<div class=\"footer-section\">
\t\t\t\t\t<h4 class=\"title\"><b>Категории</b></h4>
\t\t\t\t\t
\t\t\t\t\t{% for category in categories|batch(3) %}
\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t{% for column in category %}
\t\t\t\t\t\t\t\t<li><a href=\"{{column.url}}\">{{column.name}}</a></li>
\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t</ul>
\t\t\t\t\t{% endfor %}
\t\t\t\t\t
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"col-lg-4 col-md-6\">
\t\t\t\t<div class=\"footer-section\">
\t\t\t\t\t
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
</footer>", "C:\\local\\OpenServer\\domains\\kaktv/themes/responsiv-clean/partials/site/footer.htm", "");
    }
}
