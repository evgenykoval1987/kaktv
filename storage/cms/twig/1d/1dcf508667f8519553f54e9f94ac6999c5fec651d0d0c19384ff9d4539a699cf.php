<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\local\OpenServer\domains\kaktv/plugins/saurabhdhariwal/comments/components/comments/item.htm */
class __TwigTemplate_504847f241192ff1b897734f9a8170589f89c17e98e242833fb6ea249d05a1d6 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"comment\" id=\"comment-";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "id", [], "any", false, false, false, 1), "html", null, true);
        echo "\">
    ";
        // line 2
        if (twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "parent_id", [], "any", false, false, false, 2)) {
            // line 3
            echo "        <h5 class=\"reply-for\">ответ для <b>";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "parent", [], "any", false, false, false, 3), "name", [], "any", false, false, false, 3), "html", null, true);
            echo "</b></h5>
    ";
        }
        // line 5
        echo "    <div class=\"post-info\">
        <div class=\"left-area\">
            <img src=\"images/avatar-1-120x120.jpg\" alt=\"Profile Image\">
        </div>
        <div class=\"middle-area\">
            <b>";
        // line 10
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "name", [], "any", false, false, false, 10), "html", null, true);
        echo "</b>
            <h6 class=\"date\">";
        // line 11
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "created_at", [], "any", false, false, false, 11), "diffForHumans", [], "any", false, false, false, 11), "html", null, true);
        echo "</h6>
        </div>
        <div class=\"right-area\">
            <h5 class=\"reply-btn\" ><a href=\"#\" onclick=\"Comment.replay(event,'";
        // line 14
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "id", [], "any", false, false, false, 14), "html", null, true);
        echo "')\"><b>Ответить</b></a></h5>
        </div>
    </div>
    <p>";
        // line 17
        echo twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "content", [], "any", false, false, false, 17);
        echo "</p>
</div>";
    }

    public function getTemplateName()
    {
        return "C:\\local\\OpenServer\\domains\\kaktv/plugins/saurabhdhariwal/comments/components/comments/item.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 17,  67 => 14,  61 => 11,  57 => 10,  50 => 5,  44 => 3,  42 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"comment\" id=\"comment-{{post.id}}\">
    {% if post.parent_id %}
        <h5 class=\"reply-for\">ответ для <b>{{post.parent.name}}</b></h5>
    {% endif %}
    <div class=\"post-info\">
        <div class=\"left-area\">
            <img src=\"images/avatar-1-120x120.jpg\" alt=\"Profile Image\">
        </div>
        <div class=\"middle-area\">
            <b>{{post.name}}</b>
            <h6 class=\"date\">{{post.created_at.diffForHumans}}</h6>
        </div>
        <div class=\"right-area\">
            <h5 class=\"reply-btn\" ><a href=\"#\" onclick=\"Comment.replay(event,'{{post.id}}')\"><b>Ответить</b></a></h5>
        </div>
    </div>
    <p>{{post.content|raw}}</p>
</div>", "C:\\local\\OpenServer\\domains\\kaktv/plugins/saurabhdhariwal/comments/components/comments/item.htm", "");
    }
}
