<?php 
class Cms5dcc746d19e10760215970_9e4397da486f9c0cc93828ff8a3efa7fClass extends Cms\Classes\PageCode
{
public function onEnd()
{
    if ($this->post) {
        $this->page->meta_title = $this->post->meta_title;
        $this->page->meta_description = $this->post->meta_description;
        if ($this->post->meta_title == '')
            $this->page->meta_title = $this->post->title;
        if ($this->post->meta_description == '')
            $this->page->meta_title = $this->post->title;
    }
    else {
        return Redirect::to($this->pageUrl('404'));
    }
}
public function onStart()
{
    $this->addCss('assets/ktv/single-post-1/css/styles.css');
    $this->addCss('assets/ktv/single-post-1/css/responsive.css');
}

public function onPageComments()
{ 
    $this->commentsPost->setProperty('pageNumber', post('page'));
    $this->pageCycle();
}
}
