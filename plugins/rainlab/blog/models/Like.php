<?php namespace RainLab\Blog\Models;

use Str;
use Model;
use Url;
use Db;
use RainLab\Blog\Models\Post;
use October\Rain\Router\Helper as RouterHelper;
use Cms\Classes\Page as CmsPage;
use Cms\Classes\Theme;
use Request;

class Like extends Model
{
	public $table = 'rainlab_blog_posts_likes';

	public function scopeIp($query)
    {
        return $query->where('ip', Request::ip());
    }
}